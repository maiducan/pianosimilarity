package cz.mai.pianosimilarity.Model.Entity.Descriptors;


import java.io.PrintWriter;

import cz.mai.pianosimilarity.Presenter.FilePresenter;
import cz.mai.pianosimilarity.Model.Entity.Recording;

/**
 * Created by ducan on 04.04.2016.
 */
public abstract class Descriptors {

    protected final int                             mFrameSize;
    protected final int                             mOverlap;
    protected final int                             mSampleRate;
    protected final Recording                       mAudioFile;

    protected final FilePresenter mFilePresenter;


    protected String                                mFileFormat;
    protected PrintWriter                           mPrintWriter;
    protected String                                mFilePath;

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public                                          Descriptors(Recording audioFile, int sampleRate, int frameSize, int overlap) {

        this.mAudioFile = audioFile;
        this.mSampleRate = sampleRate;
        this.mFrameSize = frameSize;
        this.mOverlap = overlap;
        this.mFilePresenter = FilePresenter.getInstance();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                     initialise() {}
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public String                                   getFilePath() {
        return mFilePath;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
}

package cz.mai.pianosimilarity.Model.Entity.Descriptors;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.onsets.ComplexOnsetDetector;
import be.tarsos.dsp.onsets.OnsetHandler;
import cz.mai.pianosimilarity.Model.Entity.Recording;

/**
 * Created by ducan on 24.03.2016.
 */
public class Onsets extends Descriptors {


    public Onsets(Recording audioFile, int sampleRate, int frameSize, int overlap) throws FileNotFoundException {
        super(audioFile, sampleRate, frameSize, overlap);

        this.mFileFormat = ".onsets";
        this.mFilePath = mFilePresenter.getDescriptorsFolderPath() + audioFile.getFileName() + mFileFormat;
        this.mPrintWriter = new PrintWriter(mFilePath);

    }

    @Override
    public void initialise() {

        AudioDispatcher adp = AudioDispatcherFactory.fromPipe(mFilePresenter.getRecordingFolderPath() + mAudioFile.getFileName() + ".wav", mSampleRate, mFrameSize, mOverlap);

        ComplexOnsetDetector complexOnsetDetector = new ComplexOnsetDetector(mFrameSize);
        adp.addAudioProcessor(complexOnsetDetector);

        complexOnsetDetector.setHandler(new OnsetHandler() {


            @Override
            public void handleOnset(double time, double salience) {

                // Extract onset power
                //Onsets.this.mPrintWriter.println(salience);

                // Extract onset time
                Onsets.this.mPrintWriter.println(time);

            }
        });

        AudioProcessor audioProcessor = new AudioProcessor() {

            @Override
            public void processingFinished() {
                Onsets.this.mPrintWriter.close();
            }

            @Override
            public boolean process(AudioEvent audioEvent) {
                return true;
            }
        };
        adp.addAudioProcessor(audioProcessor);

        new Thread(adp).start();



    }
}

package cz.mai.pianosimilarity.Model;

import android.content.res.AssetManager;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.config.TSerializable;
import com.timeseries.TimeSeries;


import java.io.File;
import java.util.ArrayList;

import cz.mai.pianosimilarity.Model.Entity.Composer;
import cz.mai.pianosimilarity.Model.Entity.Recording;
import cz.mai.pianosimilarity.Model.Entity.Song;
import cz.mai.pianosimilarity.MyApplication;
import cz.mai.pianosimilarity.Presenter.FilePresenter;

/**
 * Created by ducan on 14.02.2016.
 */
public class Database {

    //------------------------------------------------------------------------------------------------------------------------------------------------------// VARIABLES
    private static Database mInstance;
    private static final String                             TAG = "Database";
    private static final String                             mNameDBComposers = "composers.db4o";
    private static final String                             mNameDBSongs = "songs.db4o";
    private static final String                             mNameDBRecordings = "recordings.db4o";
    private static final String                             mNameDBTimeseries = "timeseries.db4o";

    private FilePresenter                                   mFilePresenter = FilePresenter.getInstance();
    private AssetManager                                    mAssetManager = MyApplication.getAppContext().getAssets();

    private static ObjectContainer                          mDBComposers;
    private static ObjectContainer                          mDBSongs;
    private static ObjectContainer                          mDBRecordings;
    private static ObjectContainer                          mDBTimeseries;

    private ArrayList<TimeSeries>                           mTimeSeriesList;


    //------------------------------------------------------------------------------------------------------------------------------------------------------// METHODS
    /**
     * Constructor / private
     */
    private                                                 Database() {}
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Method for getting Database singleton instance
     * @return
     */
    public static Database                                  getInstance() {

        if (mInstance == null) mInstance = new Database();
        return mInstance;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Create databases
     */
    public void                                             initDB4O() {

        String dir = mFilePresenter.getDatabaseFolderPath();

        //String extDir = Environment.getExternalStorageDirectory().toString() + "/";

        mDBComposers = db(dir + mNameDBComposers, mDBComposers);
        mDBSongs = db(dir + mNameDBSongs, mDBSongs);
        mDBRecordings = db(dir + mNameDBRecordings, mDBRecordings);
        mDBTimeseries = db(dir + mNameDBTimeseries, mDBTimeseries);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Create or open the database
     */
    public ObjectContainer                                  db(String filePath, ObjectContainer oc) {



        try {
            if (oc == null || oc.ext().isClosed()) {
                oc = Db4oEmbedded.openFile(filePath);
                Log.d(TAG, "Opened new database connection");
            }

            return oc;

        } catch (Exception ie) {
            Log.e(TAG, "Unable to open database");
            return null;
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------// GETTERS

    /**
     * Return AssetManager for acces to assets
     * @return
     */
    public AssetManager                                     getAssetManager() {
        return mAssetManager;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Init descriptors to memory
     */
    public void                                             initDescriptors() {


        mTimeSeriesList = new ArrayList<>();
        final ObjectSet<Song> listSong = getSongDatabase();

        new Thread(new Runnable() {
            @Override
            public void run() {

                while(listSong.hasNext()) {
                    Song tmp = listSong.next();
                    String songName = tmp.getTitle();
                    int songID = tmp.getID();

                    String dir = mFilePresenter.getDescriptorsFolderPath();
                    File f = new File(dir + songName + ".pitch_1024");
                    if (f.exists() == false || f.length() == 0) {
                        continue;
                    }

                    TimeSeries tsEachSong = new TimeSeries(dir + songName + ".pitch_1024", false, false, ',');
                    tsEachSong.setID(songID);
                    // Didnt work to add to db4o :(
                    //addTimeSerie(tsNew);

                    mTimeSeriesList.add(tsEachSong);
                    Log.d("velikost", String.valueOf(songID));
                }

            }
        }).start();


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Return List of Recordings
     * @return
     */
    public ObjectSet<Recording>                             getRecordingDatabase() {

        ObjectSet<Recording> result = mDBRecordings.query(Recording.class);
        return result;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Search for recording instance by fileName
     * @param fileName
     * @return
     */
    public Recording                                        getRecordingByFileName(String fileName) {

        ObjectSet<Recording> res = mDBRecordings.queryByExample(new Recording(fileName));
        return res.next();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Return List of Composers
     * @return
     */
    public ObjectSet<Composer>                              getComposerDatabase() {

        ObjectSet<Composer> result = mDBComposers.query(Composer.class);
        return result;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Return List of Songs
     * @return
     */
    public ObjectSet<Song>                                  getSongDatabase() {

        ObjectSet<Song> result = mDBSongs.query(Song.class);
        return result;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Search for Song instance by its ID
     * @return
     */
    public Song                                             getSongByID(int id) {

        ObjectSet<Song> res = mDBSongs.queryByExample(new Song(id, null, null));
        return res.next();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Return List of Timeseries of songs
     * @return
     */
    public ObjectSet<TimeSeries>                            getTimeseriesDatabase() {

        ObjectSet<TimeSeries> result = mDBTimeseries.query(TimeSeries.class);
        return result;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Get ArrayList of TimeSeries from memory, not database - db didnt work
     * @return
     */
    public ArrayList                                        getTimeSeriesList() {
        return mTimeSeriesList;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------// ADD FUNCTIONS

    /**
     * Add new composer to database
     * @param composer
     */
    public void                                             addComposer(Composer composer) {
        mDBComposers.store(composer);
        mDBComposers.commit();


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Add new song to database
     * @param song
     */
    public void                                             addSong(Song song) {
        mDBSongs.store(song);
        mDBSongs.commit();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Add new recording to database
     * @param recording
     */
    public void                                             addRecording(Recording recording) {
        mDBRecordings.store(recording);
        mDBRecordings.commit();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Add new time-serie to database
     */
    public void                                             addTimeSerie(TimeSeries timeSeries) {

        mDBTimeseries.store(timeSeries);
        mDBTimeseries.commit();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------// DELETE FUNCTIONS

    /**
     * Delete specific recording
     * @param rec
     */
    public void                                             deleteRecording(Recording rec) {
        mDBRecordings.delete(rec);
        mDBRecordings.commit();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Delete all recordings in database
     */
    public void                                             deleteAllRecordings() {

        while (getRecordingDatabase().hasNext()) {

            Recording delRec = getRecordingDatabase().next();
            mDBRecordings.delete(delRec);
            mDBRecordings.commit();

        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Delete all timeseries in database
     */
    public void                                             deleteAllTimeseries() {

        while (getTimeseriesDatabase().hasNext()) {

            TimeSeries delTS = getTimeseriesDatabase().next();
            mDBTimeseries.delete(delTS);
            mDBTimeseries.commit();

        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Close all databases after app finish
     */
    public void                                             closeDatabase() {
        mDBComposers.close();
        mDBSongs.close();
        mDBRecordings.close();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//






}

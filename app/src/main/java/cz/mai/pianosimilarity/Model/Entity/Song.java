package cz.mai.pianosimilarity.Model.Entity;


import java.util.ArrayList;

/**
 * Created by ducan on 07.04.2016.
 */
public class Song {

    private int                                 mID;
    private String                              mTitle;
    private Composer                            mComposer;
    private Recording                           mRecording;


    //------------------------------------------------------------------------------------------------------------------------------------------------------// CONSTRUCTOR
    public                                      Song(int id, String title, Composer composer) {
        this.mID = id;
        this.mTitle = title;
        this.mComposer = composer;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------// SETTERS
    public void                                 setID(int ID) {
        this.mID = ID;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                 setTitle(String mTitle) {
        this.mTitle = mTitle;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                 setComposer(Composer mComposer) {
        this.mComposer = mComposer;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                 setRecording(Recording mRecording) {
        this.mRecording = mRecording;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    //------------------------------------------------------------------------------------------------------------------------------------------------------// GETTERS
    public int                                  getID() {
        return this.mID;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public String                               getTitle() {
        return mTitle;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public Composer                             getComposer() {
        return mComposer;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public Recording                            getRecording() {
        return mRecording;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
}

package cz.mai.pianosimilarity.Model.Entity.Descriptors;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.android.AndroidFFMPEGLocator;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.mfcc.MFCC;
import cz.mai.pianosimilarity.Model.Entity.Recording;
import cz.mai.pianosimilarity.MyApplication;

/**
 * Created by ducan on 24.03.2016.
 */
public class MFCCs extends Descriptors {


    public MFCCs(Recording audioFile, int sampleRate, int frameSize, int overlap) throws FileNotFoundException {
        super(audioFile, sampleRate, frameSize, overlap);

        this.mFileFormat = ".mfcc";
        this.mFilePath = mFilePresenter.getDescriptorsFolderPath() + audioFile.getFileName() + mFileFormat;
        this.mPrintWriter = new PrintWriter(mFilePath);

        //Init FFMPEG lib for feature extraction
        new AndroidFFMPEGLocator(MyApplication.getAppContext());

    }

    @Override
    public void initialise() {

        AudioDispatcher adp = AudioDispatcherFactory.fromPipe(mFilePresenter.getRecordingFolderPath() + mAudioFile.getFileName() + ".wav", mSampleRate, mFrameSize, mOverlap);

        final int numOfMFCC = 13;
        final int numOfMelFilters = 20;
        final float lowFilterFreq = 30;
        final float upperFilterFreq = 3000;

        final MFCC mfcc = new MFCC(mFrameSize, mSampleRate, numOfMFCC, numOfMelFilters, lowFilterFreq, upperFilterFreq);

        adp.addAudioProcessor(mfcc);
        adp.addAudioProcessor(new AudioProcessor() {

            @Override
            public boolean process(AudioEvent audioEvent) {

                float [] windowMFCC = mfcc.getMFCC();
                double sum = 0;

                for (int i = 1; i <= numOfMFCC; i++) {
                    sum  += windowMFCC[i];
                }

                double mean = sum / numOfMFCC;
                mPrintWriter.println(mean);

                return true;
            }

            @Override
            public void processingFinished() {

                mPrintWriter.close();

            }

        });


        new Thread(adp).start();



    }
}

package cz.mai.pianosimilarity.Model.Entity.Descriptors;

import android.os.Environment;
import android.os.Message;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.TreeMap;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.android.AndroidFFMPEGLocator;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;
import cz.mai.pianosimilarity.Presenter.ThreadPresenter;
import cz.mai.pianosimilarity.MyApplication;
import cz.mai.pianosimilarity.Model.Entity.Recording;

/**
 * Created by ducan on 21.03.2016.
 */
public class PitchContour extends Descriptors{

    private static final String                 TAG = "PitchContour";
    private static final int                    COMPARE = 3;

    private TreeMap<Double, float[]>            features;


    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public                                      PitchContour(Recording audioFile, int sampleRate, int frameSize, int overlap) throws FileNotFoundException {

        super(audioFile, sampleRate, frameSize, overlap);

        //this.features = new TreeMap<Double, float[]>();
        this.mFileFormat = ".pitch_1024";
        this.mFilePath = mFilePresenter.getDescriptorsFolderPath() + audioFile.getFileName() + mFileFormat;
        this.mPrintWriter = new PrintWriter(mFilePath);

        //Init FFMPEG lib for feature extraction
        new AndroidFFMPEGLocator(MyApplication.getAppContext());

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    @Override
    public void                                 initialise() {

        AudioDispatcher adp = AudioDispatcherFactory.fromPipe(mFilePresenter.getRecordingFolderPath() + mAudioFile.getFileName() + ".wav", mSampleRate, mFrameSize, mOverlap);

        final double timeLag = mFrameSize / 44100.0;

        //final TreeMap<Double, float[]> fe = new TreeMap<Double, float[]>();
        adp.addAudioProcessor(new PitchProcessor(
                PitchProcessor.PitchEstimationAlgorithm.FFT_YIN, 44100, mFrameSize, new PitchDetectionHandler() {

                    public void handlePitch(
                            PitchDetectionResult pitchDetectionResult,
                            AudioEvent audioEvent) {
                        if (pitchDetectionResult.isPitched()) {
                            float[] pitch = new float[1];
                            pitch[0] = (float) pitchDetectionResult.getPitch();
                            //fe.put(audioEvent.getTimeStamp() - timeLag, pitch);

                            // Print data to the file
                            PitchContour.this.mPrintWriter.println(pitch[0]);
                        }
                    }
                }));
        adp.addAudioProcessor(new AudioProcessor() {

            @Override
            public void processingFinished() {
                //PitchContour.this.features = fe;
                PitchContour.this.mPrintWriter.close();

                // Extraction finished
                ThreadPresenter mThreadHandle = ThreadPresenter.getInstance();
                Message msg = Message.obtain();
                msg.what = COMPARE;
                mThreadHandle.sendMessage(msg);

            }

            @Override
            public boolean process(AudioEvent audioEvent) {
                return true;
            }

        });

        new Thread(adp).start();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Debug
     * @return
     */
    public TreeMap<Double, float[]>             getPitchContour() {
        return this.features;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//


}

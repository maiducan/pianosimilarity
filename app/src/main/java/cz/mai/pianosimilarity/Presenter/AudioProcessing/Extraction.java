package cz.mai.pianosimilarity.Presenter.AudioProcessing;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.TreeMap;

import cz.mai.pianosimilarity.Model.Entity.Descriptors.MFCCs;
import cz.mai.pianosimilarity.Model.Entity.Descriptors.Onsets;
import cz.mai.pianosimilarity.Model.Entity.Recording;
import cz.mai.pianosimilarity.Model.Entity.Descriptors.PitchContour;


/**
 * Created by ducan on 14.02.2016.
 */
public class Extraction {

    private Recording                                   mRec;
    private PitchContour                                mPitchContour;
    private MFCCs                                       mMFCCs;
    private Onsets                                      mOnsets;

    private int                                         mDescriptorType;

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public                                              Extraction(Recording rec) {

        this.mRec = rec;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Extracts descriptors, type depends on the parameter
     *
     * PitchContour = 1
     * MFCCs = 2
     * Onsets = 3
     *
     * @param value
     */
    public void                                         extractDescriptor(int value) {

        mDescriptorType = value;

        switch (value) {
            case 1:     extractPitchContour(); break;
            case 2:     extractMFCC(); break;
            case 3:     extractOnsets(); break;
            default:    extractPitchContour(); break;
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Extract the serie of frequencies (pitch contour) of recording
     */
    public void                                         extractPitchContour() {

        try {
            mPitchContour = new PitchContour(this.mRec, 44100, 1024, 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mPitchContour.initialise();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Extract Mel-frequency cepstral coefficients
     */
    public void                                         extractMFCC() {

        try {
            mMFCCs = new MFCCs(this.mRec, 44100, 1024, 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mMFCCs.initialise();


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Extracts onsets
     */
    public void                                         extractOnsets() {

        try {
            mOnsets = new Onsets(this.mRec, 44100, 1024, 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mOnsets.initialise();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Return file with descriptors
     * @return
     */
    public String                                       getDescriptorsFilePath() {

        String filePath = "";

        switch (mDescriptorType) {
            case 1: filePath = this.mPitchContour.getFilePath(); break;
            case 2: filePath = this.mMFCCs.getFilePath(); break;
            case 3: filePath = this.mOnsets.getFilePath(); break;
        }

        return filePath;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * DEBUG MODE / Prints pitch contour - key(timestamp) + value (pitch)
     */
    public void                                         printPitchContour() {
        DecimalFormat df = new DecimalFormat("#.##");

        for (TreeMap.Entry<Double, float[]> entry : this.mPitchContour.getPitchContour().entrySet()) {
            Log.d("Extraction", "Key: " + df.format(entry.getKey()) + ". Value: " + entry.getValue()[0]);
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Returns size of the extraction file
     * @return
     */
    public float                                        getExtractionSize() {

        // Get file with descriptors
        String filePathDescriptors = getDescriptorsFilePath();
        float descriptorsSize = new File(filePathDescriptors).length();

        return descriptorsSize;

    }



}

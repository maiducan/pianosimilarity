package cz.mai.pianosimilarity.Presenter;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.MyApplication;


/**
 * Created by ducan on 09.02.2016.
 */
public class FilePresenter {

    //------------------------------------------------------------------------------------------------------------------------------------------------------// VARIABLES
    private static FilePresenter mInstance;
    private static final String                             TAG = "FilePresenter";

    private Context                                         mContext = MyApplication.getAppContext();
    private AssetManager                                    mAssetManager = mContext.getAssets();

    private final String                                    mDatabaseFolderName = "Database";
    private final String                                    mRecordingsFolderName = "Recordings";
    private final String                                    mDescriptorsFolderName = "Descriptors";

    //------------------------------------------------------------------------------------------------------------------------------------------------------// METHODS
    // Singleton
    private                                                 FilePresenter() {}
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public static FilePresenter                             getInstance() {

        if (mInstance == null) mInstance = new FilePresenter();
        return mInstance;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             createDirectories() {

        new File(mContext.getFilesDir(), mDatabaseFolderName).mkdir();
        new File(mContext.getFilesDir(), mRecordingsFolderName).mkdir();
        new File(mContext.getFilesDir(), mDescriptorsFolderName).mkdir();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             deleteRecordingFile(String fileName) {

        new File(getRecordingFolderPath(), fileName + ".wav").delete();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             deleteAllRecordings() {

        File recDir = new File(getRecordingFolderPath());

        if (recDir.isDirectory()) {
            String[] children = recDir.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(recDir, children[i]).delete();
            }
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             deleteDescriptorFile(String fileName) {
        new File(getDescriptorsFolderPath(), fileName + ".pitch_1024").delete();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             deleteDatabaseFile(String fileName) {

        new File(getDatabaseFolderPath(), fileName).delete();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------// GETTERS
    public String                                           getDatabaseFolderPath() {
        return mContext.getFilesDir().getAbsolutePath() + "/" + mDatabaseFolderName + "/";
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public String                                           getRecordingFolderPath() {
        return mContext.getFilesDir().getAbsolutePath() + "/" + mRecordingsFolderName + "/";
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public String                                           getDescriptorsFolderPath() {
        return mContext.getFilesDir().getAbsolutePath() + "/" + mDescriptorsFolderName + "/";
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             copyAssetFileToMemory(String dir, String fileName) {


        InputStream in = null;
        OutputStream out = null;
        try {
            in = mAssetManager.open(dir + "/" + fileName);

            switch (dir) {
                case "db4o": out = new FileOutputStream(getDatabaseFolderPath() + fileName); break;
                case "descriptors": out = new FileOutputStream(getDescriptorsFolderPath() + fileName); break;
                case "recordings": out = new FileOutputStream(getRecordingFolderPath() + fileName); break;
            }

            copyFile(in, out);
            in.close();
            out.flush();
            out.close();

        } catch(Exception e) {
            Log.e(TAG, e.getMessage());
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             copyDescriptorsToMemory() {

        mAssetManager = mContext.getAssets();
        int databaseSize = Database.getInstance().getSongDatabase().size();
        String[] descriptorList = new String[databaseSize];

        try {
            descriptorList = mAssetManager.list("descriptors");
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String descriptorFileName : descriptorList) {

            copyAssetFileToMemory("descriptors", descriptorFileName);

        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    private void                                            copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
            Log.d(TAG, String.valueOf(read));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//


}

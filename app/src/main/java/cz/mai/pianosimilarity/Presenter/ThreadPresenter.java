package cz.mai.pianosimilarity.Presenter;

import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.timeseries.TimeSeries;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeMap;

import cz.mai.pianosimilarity.Activity.Components.DatabaseFragments.SongsFragment;
import cz.mai.pianosimilarity.Activity.RecordDetailActivity;
import cz.mai.pianosimilarity.Activity.RecordActivity;
import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.Model.Entity.Recording;
import cz.mai.pianosimilarity.Model.Entity.Song;
import cz.mai.pianosimilarity.MyApplication;
import cz.mai.pianosimilarity.Presenter.AudioComparison.Comparison;
import cz.mai.pianosimilarity.Presenter.AudioProcessing.Extraction;

/**
 * Created by ducan on 24.03.2016.
 */
public class ThreadPresenter extends Handler {

    private static final String                     TAG = "ThreadPresenter";
    private static ThreadPresenter                  mInstance;

    private static final int                        RECORD_AUDIO = 0;
    private static final int                        STOP_RECORDING = 1;
    private static final int                        EXTRACT_DESCRIPTORS = 2;
    private static final int                        COMPARE = 3;
    private static final int                        SHOW_RESULTS = 5;

    private int                                     mComparisonType;
    private int                                     mSongID;

    private Database                                mDatabase;
    private RecordPresenter                         mRecordPresenter;

    private Extraction                              mExtraction;
    private Comparison                              mComparison;

    private Recording                               mRec;


    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    private                                         ThreadPresenter() {

        mDatabase = Database.getInstance();
        mRecordPresenter = RecordPresenter.getInstance();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public static ThreadPresenter                   getInstance() {

        if (mInstance == null) mInstance = new ThreadPresenter();
        return mInstance;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public void                                     handleMessage(Message msg) {

            switch (msg.what){

                // Record audio
                case RECORD_AUDIO:          mComparisonType = msg.arg1;
                                            mSongID = msg.arg2;
                                            runRecording();
                                            break;

                // Stop recording
                case STOP_RECORDING:        runStopRecording(); break;
                // Extract descriptors from audio
                case EXTRACT_DESCRIPTORS:   runExtraction(); break;
                // Compare with database or selected song
                case COMPARE:               runComparison(); break;
                // Show results
                case SHOW_RESULTS:          showResults(); break;

            }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Method calls RecordPresenter, launch recording and inform user on screen
     */
    private void                                    runRecording() {

        // Set components on display
        switch (mComparisonType) {
            case 0: RecordActivity.setRecordingState(); break;
            case 1: SongsFragment.setRecordingState(); break;
        }

        Log.d("comparison", "Typ: " + String.valueOf(mComparisonType));

        // Call recordPresenter;
        mRecordPresenter.record();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Method stop recording and send message to extract descriptors
     */
    private void                                    runStopRecording() {

        // Inform user about actions
        switch (mComparisonType) {
            case 0: RecordActivity.setStopRecordingState(); break;
            case 1: SongsFragment.setStopRecordingState(); break;
        }

        // Stop recording, add to database
        mRecordPresenter.stop();
        mRec = mRecordPresenter.getRecording();

        // Send message to extract descriptors
        Message msg = Message.obtain();
        msg.what = EXTRACT_DESCRIPTORS;
        sendMessage(msg);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Method make instance of Extraction and extract specified descriptor
     */
    private void                                    runExtraction() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                mExtraction = new Extraction(mRec);


                /**
                 * Choose descriptor you want to extract
                 *
                 * PitchContour = 1
                 * MFCCs = 2
                 * Onsets = 3
                 *
                 */

                mExtraction.extractDescriptor(1);

            }
        }).start();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Choose, whether to compare song with database or selected song
     */
    private void                                    runComparison() {

        switch (mComparisonType) {
            case 0: runComparisonDatabase(); break;
            case 1: runComparisonSong(); break;
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Compare recording with database
     * Creates Compare instance, timesries, inform user on screen
     */
    private void                                    runComparisonDatabase() {

        // Check, if user recorded anything
        if (mExtraction.getExtractionSize() == 0) {
            RecordActivity.setNothingRecordedState();
            return;
        }

        // Otherwise continue, inform user on Activity, app is going to compare
        RecordActivity.setComparisonState();

        // Create new Comparison instance and set number of threads to compute
        int numOfThreads = 4;
        mComparison = new Comparison(numOfThreads);

        // Create timeseries from extraction file
        final TimeSeries newTimeserie = new TimeSeries(mExtraction.getDescriptorsFilePath(), false, false, ',');

        // Run comparison with database
        mComparison.compareWithDatabase(newTimeserie);

        //-----------------------------------------------------------//
        // Debug mode - print PitchContour
        //mExtraction.printPitchContour();


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Compare recording with specified song
     */
    private void                                    runComparisonSong() {

        // Check, if user recorded anything
        if (mExtraction.getExtractionSize() == 0) {
            SongsFragment.setNothingRecordedState();
            return;
        }

        // Otherwise continue, inform user on Activity, app is going to compare
        SongsFragment.setComparisonState();

        int numOfThreads = 1;
        mComparison = new Comparison(numOfThreads);

        // Create timeseries from extraction file
        final TimeSeries newTimeserie = new TimeSeries(mExtraction.getDescriptorsFilePath(), false, false, ',');

        // Run comparison with song
        mComparison.compareWithSong(newTimeserie, mSongID);

        Log.d("comparison", "Songa: " + String.valueOf(mSongID));


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Get results from comparing and show it to user
     */
    private void                                    showResults() {

        switch (mComparisonType) {
            case 0: showResultsSearch(); break;
            case 1: showResultsSong(); Log.d("comparison", "showResults()"); break;
        }


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    private void                                    showResultsSearch() {

        // Get top similar songs and add them to recording
        TreeMap<Double, Song> topSimilarSongs = mComparison.getBestResults();
        mRec.setSimilarSongs(topSimilarSongs);
        mRec.setScore(100 - topSimilarSongs.firstKey().intValue());
        mDatabase.addRecording(mRec);

        /* Invoke activity RecordDetailActivity and show results to user */
        Intent recordDetailActivity = new Intent(MyApplication.getAppContext(), RecordDetailActivity.class);
        recordDetailActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        recordDetailActivity.putExtra("recording_name", mRec.getFileName());
        MyApplication.getAppContext().startActivity(recordDetailActivity);

        // Stop dialog
        RecordActivity.mProgressDialog.dismiss();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    private void                                     showResultsSong() {

        double res = mComparison.getSongResult();
        int score = 100 - (int)res;
        SongsFragment.setShowScoreState(score);

        Log.d("comparison", "showResultSong()");

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

}

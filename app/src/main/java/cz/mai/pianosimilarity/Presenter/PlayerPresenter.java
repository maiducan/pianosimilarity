package cz.mai.pianosimilarity.Presenter;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by ducan on 08.02.2016.
 */
public class PlayerPresenter {

    private static MediaPlayer                          mMediaPlayer;


    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public PlayerPresenter() {

        if (mMediaPlayer == null) mMediaPlayer = new MediaPlayer();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                         setSong(AssetFileDescriptor afd) {
        try {
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                         setSong(String path) {
        try {
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                         play() {

        try {
            mMediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mMediaPlayer.start();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                         pause() {

        mMediaPlayer.pause();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                         stop() {

        mMediaPlayer.stop();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//



}

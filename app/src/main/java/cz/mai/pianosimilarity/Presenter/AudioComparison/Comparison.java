package cz.mai.pianosimilarity.Presenter.AudioComparison;

import android.app.Activity;
import android.os.Message;
import android.support.v7.util.SortedList;
import android.util.Log;
import android.view.View;

import com.db4o.ObjectSet;
import com.dtw.FastDTW;
import com.timeseries.TimeSeries;
import com.util.DistanceFunction;
import com.util.DistanceFunctionFactory;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import EDU.purdue.cs.bloat.benchmark.Times;
import EDU.purdue.cs.bloat.tree.Tree;
import cz.mai.pianosimilarity.Activity.Components.DatabaseFragments.SongsFragment;
import cz.mai.pianosimilarity.Activity.RecordActivity;
import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.Model.Entity.Song;
import cz.mai.pianosimilarity.Presenter.AudioProcessing.Extraction;
import cz.mai.pianosimilarity.Presenter.ThreadPresenter;

/**
 * Created by ducan on 10.02.2016.
 */
public class Comparison {

    private static final String                     TAG = "Comparison";
    private static final int                        SHOW_RESULTS = 5;

    private TimeSeries                              mRecordingTS;
    private ObjectSet<TimeSeries>                   mDatabaseTS;
    private ArrayList<TimeSeries>                   mTimeSeriesList;

    private final Database                          mDatabase;
    private final ThreadPresenter                   mThreadHandler = ThreadPresenter.getInstance();

    private static DistanceFunction                 mDistanceFunction = DistanceFunctionFactory.getDistFnByName("EuclideanDistance");

    private Thread[]                                mThreads;
    private final int                               mThreadCount;

    private double                                  mDatabaseSize;
    private double                                  mDistanceSum;

    private TreeMap<Double, Integer>[]              mSemiResults;
    private Map<Double, Integer>                    mResults;
    private TreeMap<Double, Song>                   mTopResults;


    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public                                          Comparison(int threadCount) {

        mThreadCount = threadCount;
        mThreads = new Thread[mThreadCount];
        mSemiResults = new TreeMap[mThreadCount];
        mResults = Collections.synchronizedMap(new TreeMap<Double, Integer>());
        mTopResults = new TreeMap<>();

        mDatabase = Database.getInstance();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Method compares users recording timeserie with database of timeseries - using FastDTW algorithm
     * @param myTS
     */
    public void                                     compareWithDatabase(TimeSeries myTS) {

        mRecordingTS = myTS;
        //mDatabaseTS = mDatabase.getTimeseriesDatabase();
        mTimeSeriesList = mDatabase.getTimeSeriesList();
        mDatabaseSize = mTimeSeriesList.size();

        // Loop for thread launch
        for (int i = 0; i < mThreadCount; i++) {

            final int threadNum = i;
            mThreads[threadNum] = new Thread() {

                @Override
                public void run() {

                    int compareNum = 0;
                    double sumDistance = 0;
                    double distance;

                    mSemiResults[threadNum] = new TreeMap<>();

                    // Loop for comparison
                    while(compareNum < mDatabaseSize) {

                        if (compareNum % mThreadCount == threadNum) {

                            TimeSeries eachTimeSerie = mTimeSeriesList.get(compareNum);
                            distance = FastDTW.getWarpDistBetween(mRecordingTS, eachTimeSerie, mDistanceFunction);
                            sumDistance += distance;

                            Log.d(TAG, threadNum + ". vlakno: " + String.valueOf(compareNum) + ": " + String.valueOf(distance));

                            mSemiResults[threadNum].put(distance, eachTimeSerie.getID());
                        }

                        compareNum++;

                        // Comparison finished, merge all semiresults and send message
                        if (compareNum == mDatabaseSize) {

                            synchronized (mResults) {
                                mDistanceSum += sumDistance;
                                mResults.putAll(mSemiResults[threadNum]);
                            }

                            if (threadNum == 0) {

                                int cnt = 0;
                                for (Thread threads : mThreads) {

                                    if (cnt == 0) continue;
                                    cnt++;

                                    try {
                                        threads.join();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                }

                                // Send message
                                Message msg = Message.obtain();
                                msg.what = SHOW_RESULTS;
                                mThreadHandler.sendMessage(msg);
                            }
                        }

                    }

                }
            };

            mThreads[threadNum].start();

            Log.d(TAG, "THREAD COMPARE: " + i);
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Method compares users recording with specified song (timeserie vs. timeserie instance)
     * @param myTS
     */
    public void                                     compareWithSong(TimeSeries myTS, int songID) {

        mRecordingTS = myTS;
        mTimeSeriesList = mDatabase.getTimeSeriesList();
        TimeSeries songTS = mTimeSeriesList.get(songID);
        int cnt = 0;

        while (songTS.getID() != songID) {
            // Less timeseries than songs, thats why ID can mismatch
            cnt++;

            if (songID-cnt == -1) {
                SongsFragment.mProgressDialog.dismiss();
                SongsFragment.mTextNothingRecorded.setText("Píseň nenalezena :(");
                SongsFragment.buttonResult.setVisibility(View.VISIBLE);
            }

            songTS = mTimeSeriesList.get(songID-cnt);

            Log.d("compareSong", String.valueOf(songID) + " vs. " + String.valueOf(songTS.getID()));

        }

        double distance = FastDTW.getWarpDistBetween(mRecordingTS, songTS, mDistanceFunction);
        mResults.put(distance, songID);

        // Send message
        Message msg = Message.obtain();
        msg.what = SHOW_RESULTS;
        mThreadHandler.sendMessage(msg);


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Method returns treemap of the most similar songs with the similarity score
     * @return
     */
    public TreeMap<Double, Song>                    getBestResults() {

        int count = 0;

        Log.d("results", "SumDistance: " + String.valueOf(mDistanceSum));

        synchronized (mResults) {
            for (Map.Entry<Double, Integer> eachResult : mResults.entrySet()) {

                double distance = eachResult.getKey();
                int songID = eachResult.getValue();

                // Search for song in database
                Song tmp = mDatabase.getSongByID(songID);

                // Convert distance to similarity value
                double similarity = convertDistanceToSimilarity(distance);

                Log.d("results", String.valueOf(similarity) + " | " + tmp.getTitle());

                // Add song to final TOP similar result list
                mTopResults.put(similarity, tmp);

                count++;
                if (count == 5) break;
            }
        }

        return mTopResults;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Convert Distance measure to similarity value (0 - 100)
     * @param value
     * @return
     */
    private double                                  convertDistanceToSimilarity(double value) {

        double meanDistance = mDistanceSum / mDatabaseSize;
        double similarity = (value / meanDistance) * 100;

        Log.d("results", "MeanDistance: " + meanDistance);

        return similarity;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Method returns the similarity score for selected song
     * @return
     */
    public double                                   getSongResult() {

        double similarity = 0;

        // This value is fixed and by the results of measurement of the average
        double meanDistance = 1000000;

        // Loop because of the structure of mResults (Collections.SynchronizedMap, not TreeMap)
        // However only 1 entrySet
        for (Map.Entry<Double, Integer> res : mResults.entrySet()) {

            double distance = res.getKey();
            similarity = (distance / meanDistance) * 100;

        }

        return similarity;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//




}

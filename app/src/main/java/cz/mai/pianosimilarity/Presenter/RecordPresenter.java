package cz.mai.pianosimilarity.Presenter;

import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.os.Environment;


import java.lang.String;

import cz.mai.pianosimilarity.Model.Entity.Recording;
import cz.mai.pianosimilarity.Presenter.AudioProcessing.RehearsalAudioRecorder;

/**
 * Created by ducan on 07.02.2016.
 */
public class RecordPresenter {

    //------------------------------------------------------------------------------------------------------------------------------------------------------// Variables
    private static RecordPresenter                  mInstance;
    private static final String                     TAG = "RecordPresenter";

    private FilePresenter                           mFilePresenter = FilePresenter.getInstance();

    private Recording                               mRec;
    private RehearsalAudioRecorder                  mAudioRecorder;
    private String                                  mDirectoryPath;
    private String                                  mRecordingName;
    private String                                  mTime;

    private Thread                                  mThread;

    private int                                     mCountRecordings;

    //------------------------------------------------------------------------------------------------------------------------------------------------------// Methods
    // Singleton
    private                                         RecordPresenter() {

        mAudioRecorder = null;
        mRecordingName = "";
        mDirectoryPath = mFilePresenter.getRecordingFolderPath();
        mCountRecordings = 0;


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public static RecordPresenter                   getInstance() {

        if (mInstance == null) mInstance = new RecordPresenter();
        return mInstance;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                     record()  {


        mThread = new Thread(new Runnable() {

            public void run() {

            mCountRecordings++;

            mTime = String.valueOf(System.currentTimeMillis() / 1000);
            mRecordingName = mDirectoryPath + mTime + "_" + mCountRecordings + ".wav";
            mRec = new Recording(mTime + "_" + mCountRecordings);

            mAudioRecorder = new RehearsalAudioRecorder(
                    RehearsalAudioRecorder.RECORDING_UNCOMPRESSED,
                    MediaRecorder.AudioSource.MIC,
                    44100,
                    AudioFormat.CHANNEL_IN_STEREO,
                    AudioFormat.ENCODING_PCM_16BIT);


            mAudioRecorder.setOutputFile(mRecordingName);
            mAudioRecorder.prepare();
            mAudioRecorder.start();

            }
        });

        mThread.start();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                     stop() {

        mAudioRecorder.stop();
        mAudioRecorder.release();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public Recording                                getRecording() {
        return mRec;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

}

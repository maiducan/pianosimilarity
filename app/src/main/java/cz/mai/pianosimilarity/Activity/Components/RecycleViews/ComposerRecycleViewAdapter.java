package cz.mai.pianosimilarity.Activity.Components.RecycleViews;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.db4o.ObjectSet;

import cz.mai.pianosimilarity.Model.Entity.Composer;
import cz.mai.pianosimilarity.R;

/**
 * Created by ducan on 15.02.2016.
 */
public class ComposerRecycleViewAdapter extends RecyclerView.Adapter<ComposerRecycleViewAdapter.ComposerViewHolder> {
    //------------------------------------------------------------------------------------------------------------------------------------------------------// VARIABLES
    private ObjectSet<Composer>                 mComposerList;

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Adapter constructor
    public ComposerRecycleViewAdapter(ObjectSet<Composer> myDataset) {
        mComposerList = myDataset;
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Create new views (invoked by the layout manager)
    @Override
    public ComposerViewHolder                       onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout._item_composer, parent, false);

        ComposerViewHolder vh = new ComposerViewHolder(v);
        return vh;
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void                                     onBindViewHolder(ComposerViewHolder item, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        item.mName.setText(mComposerList.get(position).getName());
        item.mPeriod.setText(mComposerList.get(position).getPeriod());
        //  TODO IMAGE //

    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public void                                     onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int                                      getItemCount() {
        return mComposerList.size();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//



    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Inner class holding a reference to each item (recording) of recycleview
    public static class ComposerViewHolder extends RecyclerView.ViewHolder {

        CardView                                    mCardView;
        private TextView                            mName;
        private TextView                            mPeriod;
        private ImageView                           mImage;

        //------------------------------------------------------------------------------------------------------------------------------------------------------//
        public                                      ComposerViewHolder(View item) {
            super(item);

            mCardView = (CardView) item.findViewById(R.id.cv);
            mImage = (ImageView) item.findViewById(R.id.database_image);
            mName = (TextView) item.findViewById(R.id.database_name);
            mPeriod = (TextView) item.findViewById(R.id.database_period);

            onClickListeners();

        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------//
        public void                                 onClickListeners() {


            mName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /* COMPOSER DETAIL */

                }
            });

            mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /* GO TO COMPOSER DETAIL */

                }
            });

        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
}

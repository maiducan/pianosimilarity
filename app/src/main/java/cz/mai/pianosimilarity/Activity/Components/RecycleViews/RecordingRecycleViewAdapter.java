package cz.mai.pianosimilarity.Activity.Components.RecycleViews;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

import cz.mai.pianosimilarity.Activity.RecordDetailActivity;
import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.Model.Entity.Composer;
import cz.mai.pianosimilarity.Model.Entity.Song;
import cz.mai.pianosimilarity.Presenter.FilePresenter;
import cz.mai.pianosimilarity.Presenter.PlayerPresenter;
import cz.mai.pianosimilarity.Model.Entity.Recording;
import cz.mai.pianosimilarity.R;

/**
 * Created by ducan on 12.02.2016.
 */
public class RecordingRecycleViewAdapter extends RecyclerView.Adapter<RecordingRecycleViewAdapter.RecordingViewHolder> {
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    protected ArrayList<Recording>        mRecordingList = null;
    protected TreeMap<Double, Song>       mSimilarSongsList = null;
    protected String                      mType;

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Adapter constructor
    public                              RecordingRecycleViewAdapter(ArrayList<Recording> recordingList) {
        mRecordingList = recordingList;
        mType = "recordActivity";
    }
    public                              RecordingRecycleViewAdapter(TreeMap<Double, Song> similarSongsList) {
        mSimilarSongsList = similarSongsList;
        mType = "detailActivity";
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Create new views (invoked by the layout manager)
    @Override
    public                              RecordingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout._item_recording, parent, false);

        RecordingViewHolder vh = new RecordingViewHolder(v, mType);
        return vh;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void                         onBindViewHolder(RecordingViewHolder item, int position) {

        if (mType == "recordActivity") {

            Recording eachRecording = mRecordingList.get(position);

            item.mTitle.setText(eachRecording.getFileName());
            item.mScore.setText(String.valueOf(eachRecording.getScore()) + "%");
            item.mImage.setImageResource(R.drawable.play_icon);
            item.mPosition = position;
        }

        if (mType == "detailActivity") {


            Object object = mSimilarSongsList.keySet().toArray()[position];
            Song eachSong = mSimilarSongsList.get(object);
            Double similarity = (double) object;

            Log.d("finishMo", String.valueOf(similarity.intValue()));


            item.mTitle.setText(eachSong.getTitle());
            item.mComposer.setText(eachSong.getComposer().getName());
            item.mComposerObject = eachSong.getComposer();
            item.mScore.setText((100 - similarity.intValue()) + "%");
            item.mImage.setImageResource(R.drawable.play_icon);
            item.mPosition = position;

        }


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public void                         onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int                          getItemCount() {
        switch (mType){

            case "recordActivity": return mRecordingList.size();
            case "detailActivity": return mSimilarSongsList.size();
            default: return mRecordingList.size();
        }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//


    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Inner class holding a reference to each item (recording) of recycleview
    public static class RecordingViewHolder extends RecyclerView.ViewHolder {

        CardView                        mCardView;
        private TextView                mTitle;
        private TextView                mComposer;
        private Composer                mComposerObject;
        private TextView                mScore;
        private ImageView               mImage;
        private int                     mPosition;
        private String                  mActivityType;

        private Context                 mContext;
        private FilePresenter           mFilePresenter = FilePresenter.getInstance();
        private PlayerPresenter         mPlayerPresenter;

        private boolean                 mFlagPlaying;
        private String                  mDir;
        private String                  mFormat;

        //------------------------------------------------------------------------------------------------------------------------------------------------------//
        public                          RecordingViewHolder(View item, String activityType) {
            super(item);

            mContext = item.getContext();
            mActivityType = activityType;

            mCardView = (CardView) item.findViewById(R.id.cv);
            mTitle = (TextView) item.findViewById(R.id.recording_title);
            mComposer = (TextView) item.findViewById(R.id.recording_composer);
            mScore = (TextView) item.findViewById(R.id.recording_score);
            mImage = (ImageView) item.findViewById(R.id.recording_image);

            mPlayerPresenter = new PlayerPresenter();
            mFlagPlaying = false;


            onClickListeners();

        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------//
        public void                     onClickListeners() {

            if (mActivityType == "recordActivity") {

                mTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goDetailActivity();
                    }
                });

                mScore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goDetailActivity();
                    }
                });

                mImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) { musicHandlerRecordings();
                    }
                });

            }

            if (mActivityType == "detailActivity") {

                mImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        musicHandlerSongs();
                    }
                });

            }





        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------//

        /**
         * Methods invoke the change of activity -> RecordingDetailActivity
         */
        private void                    goDetailActivity() {

            final Intent recordDetailActivity = new Intent(mContext, RecordDetailActivity.class);

            recordDetailActivity.putExtra("source", "detail");
            recordDetailActivity.putExtra("recording_name", mTitle.getText());
            recordDetailActivity.putExtra("position", mPosition);

            mContext.startActivity(recordDetailActivity);

        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------//

        /**
         * Handling music playing (after image clicked)
         */
        private void                    musicHandlerRecordings() {

            mDir = mFilePresenter.getRecordingFolderPath();
            mFormat = ".wav";

            // Start playing
            if (mFlagPlaying == false) {
                mImage.setImageResource(R.drawable.pause_icon);
                mFlagPlaying = true;

                String songPath = mDir + mTitle.getText().toString() + mFormat;
                mPlayerPresenter.setSong(songPath);
                mPlayerPresenter.play();

                // Stop playing
            } else {
                mImage.setImageResource(R.drawable.play_icon);
                mFlagPlaying = false;

                mPlayerPresenter.pause();

            }

        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------//

        private void                    musicHandlerSongs() {

            mDir = "database/";
            mFormat = ".mid";



            // Start playing
            if (mFlagPlaying == false) {
                mImage.setImageResource(R.drawable.pause_icon);
                mFlagPlaying = true;

                try {

                    AssetFileDescriptor afd = Database.getInstance().getAssetManager().openFd(mDir + mComposerObject.getName() + "/" + mTitle.getText().toString() + mFormat);
                    mPlayerPresenter.setSong(afd);
                    afd.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                mPlayerPresenter.play();

                // Stop playing
            } else {
                mImage.setImageResource(R.drawable.play_icon);
                mFlagPlaying = false;

                mPlayerPresenter.pause();

            }

        }


    }


}

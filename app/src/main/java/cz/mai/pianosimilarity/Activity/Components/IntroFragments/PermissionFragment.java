package cz.mai.pianosimilarity.Activity.Components.IntroFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import cz.mai.pianosimilarity.Activity.IntroActivity;
import cz.mai.pianosimilarity.R;

/**
 * Created by ducan on 05.04.2016.
 */
public class PermissionFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String                                         ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PermissionFragment                                    newInstance(int sectionNumber) {
        PermissionFragment fragment = new PermissionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public                                                              PermissionFragment() {
    }

    @Override
    public View                                                         onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_permission, container, false);

        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText("I'm preparing your app :)");
        textView.setTextSize(20);

        return rootView;

    }

}

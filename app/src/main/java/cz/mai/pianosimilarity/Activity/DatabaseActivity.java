package cz.mai.pianosimilarity.Activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;


import cz.mai.pianosimilarity.Activity.Components.DatabaseFragments.ComposerFragment;
import cz.mai.pianosimilarity.Activity.Components.DatabaseFragments.SongsFragment;
import cz.mai.pianosimilarity.Presenter.PlayerPresenter;
import cz.mai.pianosimilarity.R;

/**
 * Created by ducan on 10.02.2016.
 */
public class DatabaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //------------------------------------------------------------------------------------------------------------------------------------------------------// VARIABLES
    private static final String             TAG = "DatabaseActivity";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    private DatabaseFragmentPagerAdapter    mDatabasePagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager                       mViewPager;
    private TabLayout                       mTabLayout;

    private NavigationView                  navigationView;
    private Toolbar                         toolbar;
    private DrawerLayout                    drawer;
    private ActionBarDrawerToggle           toggle;

    private PlayerPresenter mPlayerPresenter = new PlayerPresenter();

    //------------------------------------------------------------------------------------------------------------------------------------------------------// METHODS
    @Override
    protected void                          onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);

        // Sets activity components
        setComponents();


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                             setComponents() {

        // Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // DrawerLayout
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Navigation
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Create the adapter that will return a fragment for each of the
        // primary sections of the activity.
        mDatabasePagerAdapter = new DatabaseFragmentPagerAdapter(getFragmentManager(), DatabaseActivity.this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.viewpager_database);
        mViewPager.setAdapter(mDatabasePagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        mTabLayout.setupWithViewPager(mViewPager);


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//


    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public void                             onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        mPlayerPresenter.stop();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public boolean                          onNavigationItemSelected(MenuItem item) {

      int id = item.getItemId();

      switch (id) {
           case R.id.recordings_activity: {

               mPlayerPresenter.stop();
               startActivity(new Intent(getApplicationContext(), RecordActivity.class));

               break;
           }

           case R.id.database_activity: {

               break;
           }

           case R.id.about_activity: {

               mPlayerPresenter.stop();
               startActivity(new Intent(getApplicationContext(), AboutActivity.class));

               break;
           }
      }

      drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
      drawer.closeDrawer(GravityCompat.START);
      return true;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//



    public class DatabaseFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;
        private String tabTitles[] = new String[] { "Skladatelé", "Písně"};
        private Context context;

        public DatabaseFragmentPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0: return ComposerFragment.newInstance(position + 1);
                case 1: return SongsFragment.newInstance(position + 1);
                default: return ComposerFragment.newInstance(position + 1);
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }
    }

}

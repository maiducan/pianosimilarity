package cz.mai.pianosimilarity.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.ImageButton;
import android.widget.TextView;

import com.db4o.ObjectSet;

import java.util.ArrayList;

import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.MyApplication;
import cz.mai.pianosimilarity.Presenter.FilePresenter;
import cz.mai.pianosimilarity.Presenter.PlayerPresenter;
import cz.mai.pianosimilarity.Presenter.RecordPresenter;
import cz.mai.pianosimilarity.Activity.Components.RecycleViews.RecordingRecycleViewAdapter;
import cz.mai.pianosimilarity.Model.Entity.Recording;
import cz.mai.pianosimilarity.Presenter.ThreadPresenter;
import cz.mai.pianosimilarity.R;
import cz.mai.pianosimilarity.Activity.Components.RecycleViews.SwipeableRecyclerViewTouchListener;

public class RecordActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //------------------------------------------------------------------------------------------------------------------------------------------------------// CONST VARIABLES
    private static final int                            MY_PERMISSIONS_REQUEST_MICROPHONE = 128;
    private static final int                            RECORD_AUDIO = 0;
    private static final int                            STOP_RECORDING = 1;
    private static final String                         TAG = "RecordActivity";

    private int                                         mPermissionMicrophone;
    //------------------------------------------------------------------------------------------------------------------------------------------------------// VARIABLES
    private DrawerLayout                                drawer;
    private ActionBarDrawerToggle                       toggle;
    private NavigationView                              navigationView;

    private static AppBarLayout                         appBarLayout;
    private static CollapsingToolbarLayout              collapsingToolbarLayout;
    private Toolbar                                     toolbar;

    private static RecyclerView                         mRecyclerView;
    private RecyclerView.Adapter                        mAdapter;
    private RecyclerView.LayoutManager                  mLayoutManager;

    private static FloatingActionButton                 buttonRecord;
    private static ImageButton                          buttonStop;
    private static ImageButton                          buttonNohingRecorded;

    private static TextView                             textNothingRecorded;
    public static ProgressDialog                       mProgressDialog;

    private FilePresenter                               mFilePresenter;
    private RecordPresenter                             mRecordPresenter;
    private Database                                    mDatabase;
    private PlayerPresenter                             mPlayerPresenter;
    private ThreadPresenter                             mThreadHandler;

    private ArrayList<Recording>                        mRecordingList;

    public static Activity                              mActivity;
    private static View                                 mView;


    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    protected void                                      onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        // Initialize presenters
        initPresenters();

        // Setting components on screen activity
        setComponents();

        // Initialize data / recordings
        initData();

        // Handling user's commands
        setListeners();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Initiate presenters
     */
    private void                                        initPresenters() {

        mFilePresenter = FilePresenter.getInstance();
        mRecordPresenter = RecordPresenter.getInstance();
        mDatabase = Database.getInstance();
        mPlayerPresenter = new PlayerPresenter();
        mThreadHandler = ThreadPresenter.getInstance();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Set class variables of components on the view for handling
     */
    private void                                        setComponents() {


        // App bar layout
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        // Collapsing toolbar
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        // Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // DrawerLayout
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Navigation
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Recycle view (recordings list)
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_record);

        // Not change layout size -> performance improval
        mRecyclerView.setHasFixedSize(true);

        // Linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Button Record
        buttonRecord = (FloatingActionButton) findViewById(R.id.buttonRecord);

        // Button StopRecording
        buttonStop = (ImageButton) findViewById(R.id.buttonStop);
        buttonStop.setVisibility(View.INVISIBLE);

        // Button StopRecording
        buttonNohingRecorded = (ImageButton) findViewById(R.id.buttonNothingRecorded);
        buttonNohingRecorded.setVisibility(View.INVISIBLE);

        // Set Activity Variables
        mActivity = this;
        mView = this.findViewById(android.R.id.content);

        // Text warnings
        textNothingRecorded = (TextView) findViewById(R.id.nothing_recorded);
        textNothingRecorded.setVisibility(View.INVISIBLE);

        // Progress dialog
        mProgressDialog = new ProgressDialog(mActivity);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Get data from database (here recordings), and fill the view
     */
    private void                                        initData() {

        mRecordingList = new ArrayList<>();

        // Get recording from database and put it in ArrayList
        ObjectSet<Recording> res = mDatabase.getRecordingDatabase();
        while (res.hasNext()) mRecordingList.add(res.next());

        // Sets adapter with datas
        mAdapter = new RecordingRecycleViewAdapter(mRecordingList);
        mRecyclerView.setAdapter(mAdapter);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------// SETTING + HANDLING

    /**
     * Button (start/stop recording) listeners
     */
    private void                                        setListeners() {

        // Record button
        buttonRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkMicrophonePermissions() == PackageManager.PERMISSION_GRANTED) {

                    // Send message to handler to record audio
                    Message msg = Message.obtain();
                    msg.what = RECORD_AUDIO;
                    msg.arg1 = 0;
                    mThreadHandler.sendMessage(msg);
                }


            }
        });

        // Stop button
        buttonStop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Send message to stop the recording
                Message msg = Message.obtain();
                msg.what = STOP_RECORDING;
                mThreadHandler.sendMessage(msg);

                mRecordingList.add(mRecordPresenter.getRecording());
                mAdapter.notifyItemInserted(mRecordingList.size() - 1);

            }
        });

        buttonNohingRecorded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonNohingRecorded.setVisibility(View.INVISIBLE);
                textNothingRecorded.setVisibility(View.INVISIBLE);
                mRecyclerView.setAlpha(1.0f);

            }
        });

        // Delete swipe action
        SwipeableRecyclerViewTouchListener swipeTouchListener =
                new SwipeableRecyclerViewTouchListener(mRecyclerView,
                        new SwipeableRecyclerViewTouchListener.SwipeListener() {
                            @Override
                            public boolean canSwipe(int position) {
                                return true;
                            }

                            @Override
                            public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {

                                 /* RE-RECORD BETTER PERFORMANCE */

                            }

                            @Override
                            public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {

                                    mFilePresenter.deleteRecordingFile(mRecordingList.get(position).getFileName());
                                    mDatabase.deleteRecording(mRecordingList.get(position));
                                    mRecordingList.remove(position);
                                    mAdapter.notifyItemRemoved(position);
                                }
                                mAdapter.notifyDataSetChanged();
                            }
                        });

        mRecyclerView.addOnItemTouchListener(swipeTouchListener);



    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Visual changes after users action on activity
     * @param view
     * @param state
     */
    public void                                         setActionComponentsVisual(View view, String state) {

        switch (state) {
            case "startRecording": {

                // State bar
                Snackbar.make(view, "Recording ...", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                // Stop button
                buttonStop.setVisibility(View.VISIBLE);

                // Record button
                buttonRecord.setVisibility(View.INVISIBLE);

                // Recording list
                mRecyclerView.setAlpha(0.1f);

                textNothingRecorded.setVisibility(View.INVISIBLE);

                break;
            }
            case "stopRecording": {

                // State bar
                Snackbar.make(view, "Stopped ...", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                // Stop button
                buttonStop.setVisibility(View.INVISIBLE);

                // Record button
                buttonRecord.setVisibility(View.VISIBLE);

                // Recording list
                mRecyclerView.setAlpha(1);


                break;
            }
            case "micDisabled": {

                // State bar
                Snackbar.make(view, "You disabled microphone :(", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                // Stop button
                buttonStop.setVisibility(View.INVISIBLE);

                // Record button
                buttonRecord.setVisibility(View.VISIBLE);

                // Recording list
                mRecyclerView.setAlpha(0.1f);

            }
        }


        // Timer

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Set nothing recorded text
     */
    public static void                                  setNothingRecordedState() {

        buttonNohingRecorded.setVisibility(View.VISIBLE);
        textNothingRecorded.setVisibility(View.VISIBLE);
        mRecyclerView.setAlpha(0.1f);
        mProgressDialog.dismiss();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Inform user about recording
     */
    public static void                                  setRecordingState() {

        // State bar
        Snackbar.make(mView, "Recording ...", Snackbar.LENGTH_LONG).setAction("Action", null).show();

        appBarLayout.setExpanded(false, true);

        // Stop button
        buttonStop.setVisibility(View.VISIBLE);

        // Record button
        buttonRecord.setVisibility(View.INVISIBLE);

        // Recording list
        mRecyclerView.setAlpha(0.1f);

        // Warning invisible
        textNothingRecorded.setVisibility(View.INVISIBLE);
        buttonNohingRecorded.setVisibility(View.INVISIBLE);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Inform user about processing his recording
     */
    public static void                                  setStopRecordingState() {

        // State bar
        Snackbar.make(mView, "Stopped ...", Snackbar.LENGTH_LONG).setAction("Action", null).show();

        // Stop button
        buttonStop.setVisibility(View.INVISIBLE);

        // Record button
        buttonRecord.setVisibility(View.VISIBLE);

        // Recording list
        mRecyclerView.setAlpha(1);

        // Dialog
        mProgressDialog = ProgressDialog.show(mActivity, "Extrahování deskriptorů", "Zpracování nahrávky ...");

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Inform user about comparing
     */
    public static void                                  setComparisonState() {

        mProgressDialog.dismiss();
        mProgressDialog = ProgressDialog.show(mActivity, "Hledání v databázi", "Probíhá porovnávání ...");

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Inform user, he didnt give a permission to record
     */
    public void                                         setPermissionNotGrantedState() {

        setActionComponentsVisual(mView, "micDisabled");
        appBarLayout.setExpanded(false, true);

        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {

                finish();
                System.exit(0);

            }
        };
        handler.postDelayed(r, 3000);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Ask for permission to use microphone
     */
    public int                                          checkMicrophonePermissions() {

        mPermissionMicrophone = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        if (mPermissionMicrophone != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_MICROPHONE);
        }

        return mPermissionMicrophone;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Permission handler - after built-in popup
     */
    @Override
    public void                                         onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_MICROPHONE: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Send message to handler to record audio
                    Message msg = Message.obtain();
                    msg.what = RECORD_AUDIO;
                    mThreadHandler.sendMessage(msg);

                } else {

                    // Permission denied, boo!
                    // Functionality will not work

                    setPermissionNotGrantedState();

                }
                return;
            }

        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    //------------------------------------------------------------------------------------------------------------------------------------------------------// NAVIGATION MENU
    @Override
    public void                                         onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        mPlayerPresenter.stop();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public boolean                                      onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.record, menu);
        return true;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public boolean                                      onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete_all) {

            mFilePresenter.deleteAllRecordings();
            mDatabase.deleteAllRecordings();
            mRecordingList.clear();
            mAdapter.notifyDataSetChanged();

            ObjectSet<Recording> afterDelete = mDatabase.getRecordingDatabase();
            Log.d("velikost", String.valueOf(afterDelete.size()));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean                                      onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.recordings_activity: {

                break;
            }

            case R.id.database_activity: {

                mPlayerPresenter.stop();
                startActivity(new Intent(getApplicationContext(), DatabaseActivity.class));

                break;
            }

            case R.id.about_activity: {

                mPlayerPresenter.stop();
                startActivity(new Intent(getApplicationContext(), AboutActivity.class));

                break;
            }
        }


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

}

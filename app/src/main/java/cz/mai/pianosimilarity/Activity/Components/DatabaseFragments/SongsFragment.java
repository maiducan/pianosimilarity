package cz.mai.pianosimilarity.Activity.Components.DatabaseFragments;



/**
 * Created by ducan on 07.04.2016.
 */
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;

import com.db4o.ObjectSet;

import java.util.ArrayList;

import cz.mai.pianosimilarity.Activity.Components.RecycleViews.SongRecycleViewAdapter;
import cz.mai.pianosimilarity.Activity.DatabaseActivity;
import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.Model.Entity.Song;
import cz.mai.pianosimilarity.Presenter.ThreadPresenter;
import cz.mai.pianosimilarity.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class SongsFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String                             ARG_SECTION_NUMBER = "section_number";
    private static final int                                STOP_RECORDING = 1;


    private static RecyclerView                             mRecyclerView;
    private RecyclerView.Adapter                            mAdapter;
    private RecyclerView.LayoutManager                      mLayoutManager;
    private SearchView                                      mSearchView;
    private TabLayout                                       mTabLayout;

    private static View                                     mRootView;
    private static ImageButton                              buttonStop;
    public static ImageButton                               buttonResult;

    private static ObjectSet<Song>                          mSongList;
    private Database                                        mDatabase = Database.getInstance();
    private ThreadPresenter                                 mThreadHandler = ThreadPresenter.getInstance();

    public static ProgressDialog                            mProgressDialog;
    private static Activity                                 mActivity;

    public static TextView                                  mTextNothingRecorded;

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static SongsFragment                             newInstance(int sectionNumber) {

        SongsFragment fragment = new SongsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public                                                  SongsFragment() {}
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public View                                             onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_songs, container, false);

        // Init song list
        initData();

        // Set view
        setComponents();

        // Listeners
        setListeners();

        return mRootView;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             setComponents() {


        mActivity = getActivity();

        // Recycle view (composers list)
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view_database);

        // Button StopRecording
        buttonStop = (ImageButton) mRootView.findViewById(R.id.buttonStopSongFragment);
        buttonStop.setVisibility(View.INVISIBLE);

        // Button StopRecording
        buttonResult = (ImageButton) mRootView.findViewById(R.id.buttonResult);
        buttonResult.setVisibility(View.INVISIBLE);

        // Not change layout size -> performance improval
        mRecyclerView.setHasFixedSize(true);

        // Linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Sets adapter with datas
        mAdapter = new SongRecycleViewAdapter(mSongList);
        mRecyclerView.setAdapter(mAdapter);

        mSearchView = (SearchView) getActivity().findViewById(R.id.searchView);
        mSearchView.setVisibility(View.VISIBLE);
        mTabLayout = (TabLayout) getActivity().findViewById(R.id.sliding_tabs);

        int id = mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) mSearchView.findViewById(id);
        textView.setTextColor(Color.WHITE);

        // Text warnings
        mTextNothingRecorded = (TextView) mRootView.findViewById(R.id.nothing_recorded);
        mTextNothingRecorded.setVisibility(View.INVISIBLE);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             initData() {

        mSongList = mDatabase.getSongDatabase();


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    private void                                            setListeners() {

        // Stop button
        buttonStop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Send message to stop the recording
                Message msg = Message.obtain();
                msg.what = STOP_RECORDING;
                mThreadHandler.sendMessage(msg);

            }
        });

        buttonResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonResult.setVisibility(View.INVISIBLE);
                mTextNothingRecorded.setVisibility(View.INVISIBLE);
                mRecyclerView.setAlpha(1.0f);

            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mTabLayout.setVisibility(View.VISIBLE);
                return true;
            }
        });


        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTabLayout.setVisibility(View.INVISIBLE);
            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                mTabLayout.setVisibility(View.INVISIBLE);

                query = query.toLowerCase();

                final ArrayList<Song> filteredList = new ArrayList<>();

                mSongList.reset();
                while (mSongList.hasNext()) {

                    Song song = mSongList.next();

                    if (song.getTitle().contains(query)) {

                        filteredList.add(song);
                    }
                }


                mAdapter = new SongRecycleViewAdapter(filteredList);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();  // data set changed
                return true;

            }

        });

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public static void                                      setRecordingState() {

        // State bar
        Snackbar.make(mRootView, "Recording ...", Snackbar.LENGTH_LONG).setAction("Action", null).show();

        // Stop button
        buttonStop.setVisibility(View.VISIBLE);

        // Recording list
        mRecyclerView.setAlpha(0.1f);

        // Warning invisible
        mTextNothingRecorded.setVisibility(View.INVISIBLE);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public static void                                      setStopRecordingState() {

        // State bar
        Snackbar.make(mRootView, "Stopped ...", Snackbar.LENGTH_LONG).setAction("Action", null).show();

        // Stop button
        buttonStop.setVisibility(View.INVISIBLE);

        // Recording list
        mRecyclerView.setAlpha(1);

        // Dialog
        mProgressDialog = ProgressDialog.show(mActivity, "Extrahování deskriptorů", "Zpracování nahrávky ...");

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public static void                                      setNothingRecordedState() {


        buttonResult.setVisibility(View.VISIBLE);
        mTextNothingRecorded.setVisibility(View.VISIBLE);
        mTextNothingRecorded.setText("Nic neslyším :(");
        mRecyclerView.setAlpha(0.1f);
        mProgressDialog.dismiss();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public static void                                      setComparisonState() {

        // Dialog
        mProgressDialog.dismiss();
        mProgressDialog = ProgressDialog.show(mActivity, "Porovnání s písní", "Prosím vyčkejte ...");


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public static void                                      setShowScoreState(int score) {

        mTextNothingRecorded.setText("Podobnost: " + score + " %");
        mTextNothingRecorded.setVisibility(View.VISIBLE);
        buttonResult.setVisibility(View.VISIBLE);
        mRecyclerView.setAlpha(0.1f);
        mProgressDialog.dismiss();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
}

package cz.mai.pianosimilarity.Activity.Components.RecycleViews;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.os.Message;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.db4o.ObjectSet;
import java.io.IOException;
import java.util.ArrayList;


import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.Presenter.PlayerPresenter;
import cz.mai.pianosimilarity.Model.Entity.Composer;
import cz.mai.pianosimilarity.Model.Entity.Song;
import cz.mai.pianosimilarity.Presenter.ThreadPresenter;
import cz.mai.pianosimilarity.R;

/**
 * Created by ducan on 07.04.2016.
 */
public class SongRecycleViewAdapter extends RecyclerView.Adapter<SongRecycleViewAdapter.SongViewHolder> implements Filterable {
    //------------------------------------------------------------------------------------------------------------------------------------------------------// VARIABLES
    private ObjectSet<Song>                         mSongList;
    private ArrayList<Song>                         mSongListFiltered;
    private boolean                                 mFiltered;

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Adapter constructor
    public                                          SongRecycleViewAdapter(ObjectSet<Song> songs) {
        mSongList = songs;
        mFiltered = false;
    }
    public                                          SongRecycleViewAdapter(ArrayList<Song> filteredSongs) {
        mSongListFiltered = filteredSongs;
        mFiltered = true;
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Create new views (invoked by the layout manager)
    @Override
    public SongViewHolder                           onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout._item_song, parent, false);

        SongViewHolder vh = new SongViewHolder(v);
        return vh;
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void                                     onBindViewHolder(SongViewHolder item, int position) {

        Song eachSong;

        if (mFiltered == true) {
            eachSong = mSongListFiltered.get(position);
        }
        else {
            eachSong = mSongList.get(position);
        }


        item.mSong = eachSong;
        item.mName.setText(eachSong.getTitle());
        item.mComposer = eachSong.getComposer();
        item.mImage.setImageResource(R.drawable.play_icon);
        item.mCompareIcon.setImageResource(R.drawable.compare_icon);

    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public void                                     onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int                                      getItemCount() {
        if (mFiltered == false) return mSongList.size();
        else return mSongListFiltered.size();
    }



    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    // Inner class holding a reference to each item (recording) of recycleview
    public static class SongViewHolder extends RecyclerView.ViewHolder {

        public CardView                             mCardView;
        private ImageView                           mImage;
        private TextView                            mName;
        private ImageView                           mCompareIcon;
        private Composer                            mComposer;
        private Song                                mSong;

        private boolean                             mFlagPlaying;

        private Database                            mDatabase = Database.getInstance();
        private AssetManager                        mAssetManager;
        private PlayerPresenter                     mPlayerPresenter;
        private String                              mDir;
        private String                              mFormat;

        private static final int                    RECORD_AUDIO = 0;
        private static final int                    STOP_RECORDING = 1;

        private ThreadPresenter                     mThreadHandler = ThreadPresenter.getInstance();


        //------------------------------------------------------------------------------------------------------------------------------------------------------//
        public                                      SongViewHolder(View item) {
            super(item);

            mCardView = (CardView) item.findViewById(R.id.cv);
            mImage = (ImageView) item.findViewById(R.id.database_image);
            mName = (TextView) item.findViewById(R.id.database_name);
            mCompareIcon = (ImageView) item.findViewById(R.id.compare_button);

            mPlayerPresenter = new PlayerPresenter();
            mAssetManager = mDatabase.getAssetManager();
            mFlagPlaying = false;
            mDir = "database/";
            mFormat = ".mid";

            onClickListeners();

        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------//
        public void                                 onClickListeners() {


            mName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    musicHandler();

                }
            });

            mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    musicHandler();


                }
            });

            mCompareIcon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    // Send message to handler to record audio
                    Message msg = Message.obtain();
                    msg.what = RECORD_AUDIO;
                    msg.arg1 = 1;
                    msg.arg2 = mSong.getID();
                    mThreadHandler.sendMessage(msg);

                }
            });

        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------//
        public void                                 musicHandler() {

            // Start playing
            if (mFlagPlaying == false) {
                mImage.setImageResource(R.drawable.pause_icon);
                mFlagPlaying = true;

                try {

                    AssetFileDescriptor afd = mAssetManager.openFd(mDir + mComposer.getName() + "/" + mName.getText().toString() + mFormat);
                    mPlayerPresenter.setSong(afd);
                    afd.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                mPlayerPresenter.play();

                // Stop playing
            } else {
                mImage.setImageResource(R.drawable.play_icon);
                mFlagPlaying = false;

                mPlayerPresenter.pause();

            }

        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------//
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public Filter getFilter() {
        return null;
    }






    //------------------------------------------------------------------------------------------------------------------------------------------------------//

}

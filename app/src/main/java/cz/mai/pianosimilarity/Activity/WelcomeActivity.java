package cz.mai.pianosimilarity.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.R;

/**
 * Created by ducan on 09.02.2016.
 */
public class WelcomeActivity extends Activity {

    private static final String             TAG = "WelcomeActivity";

    private Database mDatabase = Database.getInstance();

    private SharedPreferences               mSharedPreferences;
    private boolean                         mFlagFirstRun = true;


    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    protected void                          onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // Check if is it first run of application
        mFlagFirstRun = isFirstRun();

        // Sets welcome animation
        welcomeAnimation();

        // Initialize application
        initApplication();


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Splits states - the first init of app [copy database to memory, creating folders] vs. relaunch
     */
    private boolean                         isFirstRun() {

        // Get the info from SharedPreferences of application
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean firstrun = mSharedPreferences.getBoolean("firstrun", true);

        if (firstrun == true) {

            // Change boolean of firstrun to false
            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putBoolean("firstrun", false);
            e.commit();

            return true;

        } else return false;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Run welcome animation and separate first launch from re-launch
     */
    private void                            welcomeAnimation() {

        ImageView img_logo = (ImageView)findViewById(R.id.logo);
        Animation anim_logo = AnimationUtils.loadAnimation(this, R.anim.welcome_sr);

        img_logo.startAnimation(anim_logo);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    private void                            initApplication() {


        Thread initAppThread = new Thread(new Runnable() {
            @Override
            public void run() {

                // Go to IntroActivity (copy assets and init DB)
                if (mFlagFirstRun == true) {
                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    startActivity(new Intent(getApplicationContext(), IntroActivity.class));
                }

                // Go to RecordActivity (init DB)
                else {
                    mDatabase.initDB4O();
                    mDatabase.initDescriptors();

                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    startActivity(new Intent(getApplicationContext(), RecordActivity.class));
                }

            }
        });

        initAppThread.start();





    }

}

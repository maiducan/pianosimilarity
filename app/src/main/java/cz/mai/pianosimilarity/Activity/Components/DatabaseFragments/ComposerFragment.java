package cz.mai.pianosimilarity.Activity.Components.DatabaseFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.db4o.ObjectSet;

import cz.mai.pianosimilarity.Activity.Components.RecycleViews.ComposerRecycleViewAdapter;
import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.Model.Entity.Composer;
import cz.mai.pianosimilarity.R;

/**
 * Created by ducan on 07.04.2016.
 */
/**
 * A placeholder fragment containing a simple view.
 */
public class ComposerFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String                             ARG_SECTION_NUMBER = "section_number";

    private RecyclerView                                    mRecyclerView;
    private RecyclerView.Adapter                            mAdapter;
    private RecyclerView.LayoutManager                      mLayoutManager;

    private View                                            rootView;

    private static ObjectSet<Composer>                      mComposerList;
    private Database mDatabase = Database.getInstance();

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ComposerFragment                          newInstance(int sectionNumber) {
        ComposerFragment fragment = new ComposerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public                                                  ComposerFragment() {}
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public View                                             onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_composer, container, false);

        // Init composers list
        initData();

        // Set view
        setComponents();

        return rootView;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             initData() {

        mComposerList = mDatabase.getComposerDatabase();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void                                             setComponents() {


        // Recycle view (composers list)
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_database);

        // Not change layout size -> performance improval
        mRecyclerView.setHasFixedSize(true);

        // Linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Sets adapter with datas
        mAdapter = new ComposerRecycleViewAdapter(mComposerList);
        mRecyclerView.setAdapter(mAdapter);

        SearchView sw = (SearchView) getActivity().findViewById(R.id.searchView);
        sw.setVisibility(View.INVISIBLE);

    }


}
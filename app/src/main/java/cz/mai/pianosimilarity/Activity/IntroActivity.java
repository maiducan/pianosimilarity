package cz.mai.pianosimilarity.Activity;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import cz.mai.pianosimilarity.Activity.Components.IntroFragments.IntroFragment;
import cz.mai.pianosimilarity.Activity.Components.IntroFragments.PermissionFragment;
import cz.mai.pianosimilarity.Activity.Components.IntroFragments.VideoFragment;
import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.Presenter.FilePresenter;
import cz.mai.pianosimilarity.R;

public class IntroActivity extends Activity {

    //------------------------------------------------------------------------------------------------------------------------------------------------------// VARIABLES

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter                                        mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager                                                   mViewPager;

    private FilePresenter                                               mFilePresenter;
    private Database                                                    mDatabase;


    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    protected void                                                      onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        // Initialize presenters
        initPresenters();

        // Sets pager
        setComponents();

        // Create directories for app
        initDirectory();

        // Open the database
        initDatabase();

        // Dialog
        ProgressDialog.show(this, "Příprava aplikace ...", "Kopírují se soubory");


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Initialize presenters
     */
    private void                                                        initPresenters() {

        mFilePresenter = FilePresenter.getInstance();
        mDatabase = Database.getInstance();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Sets components (pager)
     */
    private void                                                        setComponents() {

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Create application directories
     */
    private void                                                        initDirectory() {
        mFilePresenter.createDirectories();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Copy and open the database (composers, songs, recordings, time-series)
     */
    private void                                                        initDatabase() {

        Thread database_thread = new Thread() {

            @Override
            public void run() {

                mFilePresenter.copyAssetFileToMemory("db4o", "composers.db4o");
                mFilePresenter.copyAssetFileToMemory("db4o", "recordings.db4o");
                mFilePresenter.copyAssetFileToMemory("db4o", "songs.db4o");
                mFilePresenter.copyAssetFileToMemory("recordings", "Ukazka.wav");
                mDatabase.initDB4O();

                mFilePresenter.copyDescriptorsToMemory();

                // Did not work, db4o was not able to handle TimeSeries objects  :(
                //mFilePresenter.copyAssetFileToMemory("timeseries.db4o");

                mDatabase.initDescriptors();

                try {
                    sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(getApplicationContext(), RecordActivity.class));
                finish();

            }
        };

        database_thread.start();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (position) {
                case 0: return IntroFragment.newInstance(position + 1);
                case 1: return PermissionFragment.newInstance(position + 1);
                case 2: return VideoFragment.newInstance(position + 1);
                default: return IntroFragment.newInstance(position + 1);
            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_intro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package cz.mai.pianosimilarity.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import cz.mai.pianosimilarity.R;

/**
 * Created by ducan on 10.02.2016.
 */
public class AboutActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //------------------------------------------------------------------------------------------------------------------------------------------------------// Variables
    private NavigationView                  navigationView;
    private Toolbar                         toolbar;
    private DrawerLayout                    drawer;
    private ActionBarDrawerToggle           toggle;

    //------------------------------------------------------------------------------------------------------------------------------------------------------// Methods
    @Override
    protected void                          onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        setComponents();

        //Comparison.compareWithDatabase();


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    public void                             setComponents() {

        // Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // DrawerLayout
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Navigation
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public boolean                          onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.recordings_activity: {

                startActivity(new Intent(getApplicationContext(), RecordActivity.class));

                break;
            }

            case R.id.database_activity: {

                startActivity(new Intent(getApplicationContext(), DatabaseActivity.class));

                break;
            }

            case R.id.about_activity: {
                break;
            }
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//


}

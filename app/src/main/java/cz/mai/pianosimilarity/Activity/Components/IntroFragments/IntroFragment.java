package cz.mai.pianosimilarity.Activity.Components.IntroFragments;

/**
 * Created by ducan on 05.04.2016.
 */

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cz.mai.pianosimilarity.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class IntroFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String                     ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static IntroFragment                     newInstance(int sectionNumber) {
        IntroFragment fragment = new IntroFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public                                          IntroFragment() {
    }

    @Override
    public View                                     onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_intro, container, false);

        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText("Welcome");
        textView.setTextSize(20);

        return rootView;
    }
}

package cz.mai.pianosimilarity.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import cz.mai.pianosimilarity.Activity.AboutActivity;
import cz.mai.pianosimilarity.Activity.Components.RecycleViews.RecordingRecycleViewAdapter;
import cz.mai.pianosimilarity.Activity.DatabaseActivity;
import cz.mai.pianosimilarity.Activity.RecordActivity;
import cz.mai.pianosimilarity.Model.Database;
import cz.mai.pianosimilarity.Model.Entity.Song;
import cz.mai.pianosimilarity.Presenter.FilePresenter;
import cz.mai.pianosimilarity.Presenter.PlayerPresenter;
import cz.mai.pianosimilarity.Presenter.RecordPresenter;
import cz.mai.pianosimilarity.Model.Entity.Recording;
import cz.mai.pianosimilarity.Presenter.ThreadPresenter;
import cz.mai.pianosimilarity.R;

/**
 * Created by ducan on 15.02.2016.
 */
public class RecordDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    private static final String             TAG = "RecordDetail_Activity";
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    private DrawerLayout                    drawer;
    private ActionBarDrawerToggle           toggle;
    private NavigationView                  navigationView;

    private AppBarLayout                    appBarLayout;
    private CollapsingToolbarLayout         collapsingToolbarLayout;
    private Toolbar                         toolbar;



    private FloatingActionButton            buttonRecord;
    private ImageButton                     buttonStop;

    private FilePresenter                   mFilePresenter;
    private RecordPresenter                 mRecordPresenter;
    private Database                        mDatabase;
    private PlayerPresenter                 mPlayerPresenter;

    private String                          mDirectory;
    private String                          mTitle;

    private Recording                       mRecording;

    private static RecyclerView             mRecyclerView;
    private RecyclerView.Adapter            mAdapter;
    private RecyclerView.LayoutManager      mLayoutManager;



    //------------------------------------------------------------------------------------------------------------------------------------------------------// ON CREATE
    @Override
    protected void                          onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_record);


        // Init presenters
        initPresenters();

        // Setting components on screen activity
        setComponents();

        // Initialize data
        initData();


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Initiate presenters
     */
    private void                            initPresenters() {

        mFilePresenter = FilePresenter.getInstance();
        mRecordPresenter = RecordPresenter.getInstance();
        mDatabase = Database.getInstance();
        mPlayerPresenter = new PlayerPresenter();
        mDirectory = mFilePresenter.getRecordingFolderPath();

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//

    /**
     * Get data from intents
     */
    public void                             initData() {

        Intent tmp = getIntent();
        mTitle = tmp.getStringExtra("recording_name");

        collapsingToolbarLayout.setTitle(mTitle);
        mRecording = mDatabase.getRecordingByFileName(mTitle);

        TreeMap<Double, Song> similarSongs = mRecording.getSimilarSongs();

        // Sets adapter with datas
        mAdapter = new RecordingRecycleViewAdapter(similarSongs);
        mRecyclerView.setAdapter(mAdapter);

    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------// SETTING + HANDLING
    public void                             setComponents() {

        // App bar layout
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        // Collapsing toolbar
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(mTitle);

        // Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // DrawerLayout
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Navigation
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Recycle view (recordings list)
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_record);

        // Not change layout size -> performance improval
        mRecyclerView.setHasFixedSize(true);

        // Linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//


    //------------------------------------------------------------------------------------------------------------------------------------------------------// NAVIGATION MENU
    @Override
    public void                             onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        mPlayerPresenter.stop();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public boolean                          onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.recorddetail, menu);
        return true;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public boolean                          onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean                          onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.recordings_activity: {

                mPlayerPresenter.stop();
                startActivity(new Intent(getApplicationContext(), RecordActivity.class));

                break;
            }

            case R.id.database_activity: {

                mPlayerPresenter.stop();
                startActivity(new Intent(getApplicationContext(), DatabaseActivity.class));

                break;
            }

            case R.id.about_activity: {

                mPlayerPresenter.stop();
                startActivity(new Intent(getApplicationContext(), AboutActivity.class));

                break;
            }
        }


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------//


}
